Backend (https://gitlab.com/JCRamires/teste-lyncas-backend)
- Precisa de uma instancia do mongo rodando na porta padrão (27017)
- Clonar repo
- Executar com o comando ` ./mvnw spring-boot:run`

Frontend (https://gitlab.com/JCRamires/teste-lyncas-frontend)
- Clonar o repo
- `npm install` ou `yarn install`
- `yarn start`