import React, { useState } from 'react'
import Typography from '@material-ui/core/Typography'
import Snackbar from '@material-ui/core/Snackbar'
import MuiAlert from '@material-ui/lab/Alert'
import Button from '@material-ui/core/Button'
import SaveIcon from '@material-ui/icons/Save'

import { addToFavorites } from './gateways/FavoriteGateway'

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />
}

function BookDetails(props) {
  const [showSuccessMessage, setShowSuccessMessage] = useState(false)

  async function onClickAddToFavorites() {
    await addToFavorites(
      props.googleId,
      props.title,
      props.description,
      props.imageUrl,
      props.author
    )

    setShowSuccessMessage(true)
  }

  const handleCloseMessage = (event, reason) => {
    if (reason === 'clickaway') {
      return
    }

    setShowSuccessMessage(false)
  }

  return (
    <>
      <div style={{ display: 'flex', height: '500px', width: '800px' }}>
        <div style={{ width: '30%', marginRight: '15px' }}>
          <img src={props.imageUrl} style={{ height: 'auto', width: '100%' }} />
        </div>
        <div style={{ flex: 1 }}>
          <Typography variant="h6" gutterBottom>
            {props.title}
          </Typography>
          <div style={{ overflowY: 'auto', maxHeight: '400px' }}>
            <Typography variant="body1" gutterBottom>
              {props.description}
            </Typography>
          </div>
        </div>
      </div>
      {!props.favoritesList && (
        <div>
          <Button
            onClick={onClickAddToFavorites}
            variant="contained"
            color="primary"
            size="small"
            startIcon={<SaveIcon />}
          >
            Add to favorites
          </Button>
        </div>
      )}
      <Snackbar open={showSuccessMessage} autoHideDuration={6000} onClose={handleCloseMessage}>
        <Alert onClose={handleCloseMessage} severity="success">
          Added to favorites!
        </Alert>
      </Snackbar>
    </>
  )
}

BookDetails.defaultProps = {
  favoritesList: false,
}

export default BookDetails
