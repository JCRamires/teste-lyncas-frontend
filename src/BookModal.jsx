import React, { useState, useEffect } from 'react'
import Modal from '@material-ui/core/Modal'
import { makeStyles } from '@material-ui/core/styles'

import { getBookByGoogleId } from './gateways/SearchGateway'
import { getImageUrl } from './utils'

import BookDetails from './BookDetails'

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}))

function BookModal(props) {
  const [loading, setLoading] = useState(true)
  const [bookDetails, setBookDetails] = useState()
  const classes = useStyles()
  const handleClose = () => props.onClose()

  useEffect(() => {
    // Criar a função para depois chamar ela parece loucura, mas é apenas react (https://github.com/facebook/react/issues/14326)
    async function searchBook() {
      const { data } = await getBookByGoogleId(props.googleId)
      const { title, description, imageLinks, authors } = data.volumeInfo

      setBookDetails({
        googleId: props.googleId,
        title,
        description,
        imageUrl: getImageUrl(imageLinks),
        author: authors ? authors[0] : 'Author unknown',
      })

      setLoading(false)
    }

    searchBook()
  }, [])

  return (
    <Modal open onClose={handleClose} className={classes.modal}>
      <div className={classes.paper}>
        {loading ? (
          <span>Loading...</span>
        ) : (
          <BookDetails favoritesList={props.favoritesList} {...bookDetails} />
        )}
      </div>
    </Modal>
  )
}

BookModal.defaultProps = {
  favoritesList: false,
}

export default BookModal
