import React, { memo, useState } from 'react'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import CardActions from '@material-ui/core/CardActions'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import DeleteIcon from '@material-ui/icons/Delete'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import Dialog from '@material-ui/core/Dialog'

import { deleteFromFavorites } from './gateways/FavoriteGateway'

import BookModal from './BookModal'

function BookGrid(props) {
  const [googleId, setGoogleId] = useState()
  const [bookToDelete, setBookToDelete] = useState()

  const handleClose = () => setGoogleId(undefined)
  const handleCardClick = (googleId) => () => setGoogleId(googleId)

  const handleClickDelete = (bookLink) => () => setBookToDelete(bookLink)
  const handleCancelDelete = () => setBookToDelete(undefined)
  const handleOkDelete = async () => {
    await deleteFromFavorites(bookToDelete)

    props.onDelete()
  }

  function getLoadMoreButton() {
    if (props.totalItems !== undefined && props.books.length < props.totalItems) {
      return (
        <div style={{ display: 'flex', justifyContent: 'space-around', marginTop: '25px' }}>
          <Button variant="contained" onClick={props.getNextPage}>
            Load more
          </Button>
        </div>
      )
    }
  }

  return (
    <>
      <Grid container spacing={3}>
        {props.books.map((book) => (
          <Grid key={book.googleId} item lg={2} md={3} sm={6} xs={12}>
            <Card>
              <CardActionArea onClick={handleCardClick(book.googleId)}>
                <CardMedia
                  component="img"
                  alt={book.title}
                  image={book.imageUrl}
                  title={book.title}
                  height={303}
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    {book.title}
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    by: {book.author ? book.author : 'Author unknown'}
                  </Typography>
                </CardContent>
              </CardActionArea>
              {props.favoritesList && (
                <CardActions>
                  <Button
                    onClick={handleClickDelete(book._links.self.href)}
                    size="small"
                    variant="contained"
                    color="secondary"
                    startIcon={<DeleteIcon />}
                  >
                    Delete
                  </Button>
                </CardActions>
              )}
            </Card>
          </Grid>
        ))}
      </Grid>
      {getLoadMoreButton()}
      {googleId !== undefined && (
        <BookModal favoritesList={props.favoritesList} googleId={googleId} onClose={handleClose} />
      )}
      <Dialog maxWidth="xs" open={bookToDelete !== undefined}>
        <DialogTitle>Delete from favorites?</DialogTitle>
        <DialogContent>Do you want to remove the book from you favorites list?</DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleCancelDelete} color="primary">
            Cancel
          </Button>
          <Button onClick={handleOkDelete} color="primary">
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}

BookGrid.defaultProps = {
  favoritesList: false,
}

export default memo(BookGrid)
