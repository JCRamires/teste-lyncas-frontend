import React, { useState, useEffect } from 'react'
import Snackbar from '@material-ui/core/Snackbar'
import MuiAlert from '@material-ui/lab/Alert'

import { getFavorites } from './gateways/FavoriteGateway'

import BookGrid from './BookGrid'

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />
}

function FavoritesList() {
  const [loading, setLoading] = useState(true)
  const [favorites, setFavorites] = useState([])
  const [showSuccessMessage, setShowSuccessMessage] = useState(false)

  async function loadFavorites() {
    setLoading(true)

    const { data } = await getFavorites()

    setFavorites(data._embedded.favorites)
    setLoading(false)
  }

  useEffect(() => {
    loadFavorites()
  }, [])

  function onDelete() {
    setShowSuccessMessage(true)

    loadFavorites()
  }

  const handleCloseMessage = (event, reason) => {
    if (reason === 'clickaway') {
      return
    }

    setShowSuccessMessage(false)
  }

  return (
    <div>
      {loading ? (
        <span>Loading...</span>
      ) : (
        <BookGrid onDelete={onDelete} books={favorites} favoritesList />
      )}
      <Snackbar open={showSuccessMessage} autoHideDuration={6000} onClose={handleCloseMessage}>
        <Alert onClose={handleCloseMessage} severity="success">
          Removed from favorites!
        </Alert>
      </Snackbar>
    </div>
  )
}

export default FavoritesList
