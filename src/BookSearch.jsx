import React, { useState } from 'react'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'

import { searchBookByName } from './gateways/SearchGateway'
import { volumesToBookList } from './utils'

import BookGrid from './BookGrid'

function BookSearch() {
  const [bookName, setBookName] = useState('')
  const [searchResult, setSearchResult] = useState([])
  const [totalItems, setTotalItems] = useState()

  async function onSearch(event) {
    event.preventDefault()

    const { data } = await searchBookByName(bookName)

    setSearchResult(volumesToBookList(data.items))
    setTotalItems(data.totalItems)
  }

  async function getNextPage() {
    const { data } = await searchBookByName(bookName, searchResult.length)

    setSearchResult([...searchResult, ...volumesToBookList(data.items)])
  }

  const onChangeBookName = (event) => setBookName(event.target.value)

  return (
    <>
      <form onSubmit={onSearch} noValidate autoComplete="off">
        <div style={{ display: 'flex', alignItems: 'center', marginBottom: '25px' }}>
          <TextField id="bookName" label="Book name" value={bookName} onChange={onChangeBookName} />
          <Button
            variant="contained"
            colo="primary"
            size="small"
            type="submit"
            style={{ marginLeft: '15px' }}
          >
            Search
          </Button>
        </div>
      </form>
      {searchResult.length > 0 && (
        <BookGrid getNextPage={getNextPage} totalItems={totalItems} books={searchResult} />
      )}
    </>
  )
}

export default BookSearch
