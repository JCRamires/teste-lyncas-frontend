import axios from 'axios'

export function getFavorites() {
  return axios.get('http://localhost:8080/favorites')
}

export function addToFavorites(googleId, title, description, imageUrl, author) {
  return axios.post('http://localhost:8080/favorites', {
    googleId,
    title,
    description,
    imageUrl,
    author,
  })
}

export function deleteFromFavorites(mongoDocumentLink) {
  return axios.delete(mongoDocumentLink)
}
