import axios from 'axios'

export function searchBookByName(bookName, startIndex = 0) {
  return axios.get('http://localhost:8080/bookSearch', { params: { name: bookName, startIndex } })
}

export function getBookByGoogleId(googleId) {
  return axios.get(`http://localhost:8080/book/${googleId}`)
}
