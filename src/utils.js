// Função para retornar a maior imagem fornecida pela api do google books, mas percebi que a maioria dos livros só tem thumbnail
export function getImageUrl(imageLinks) {
  if (imageLinks === null) return 'https://via.placeholder.com/200x303?text=Image+not+found'

  return (
    imageLinks.medium ||
    imageLinks.small ||
    imageLinks.thumbnail ||
    'https://via.placeholder.com/200x303?text=Image+not+found'
  )
}

export function volumesToBookList(volumes) {
  if (volumes === null || volumes === undefined) return []

  return volumes.map((volume) => ({
    googleId: volume.id,
    title: volume.volumeInfo.title,
    description: volume.volumeInfo.description,
    imageUrl: getImageUrl(volume.volumeInfo.imageLinks),
    author: volume.volumeInfo.authors ? volume.volumeInfo.authors[0] : 'Author unknown',
  }))
}
